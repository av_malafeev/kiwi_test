<?php

declare(strict_types=1);

namespace app\widgets;

use yii\base\Widget;
use yii\bootstrap\Alert;

/**
 * Class Alert
 * @package app\widgets
 */
class FlashMessage extends Widget
{
    const SEPARATOR = '<br>';

    const CATEGORY_ERROR = 'error';
    const CATEGORY_SUCCESS = 'success';
    const CATEGORY_INFO = 'info';

    private function getClassByCategory(string $category): string
    {
        switch ($category) {
            case self::CATEGORY_ERROR:
                return 'alert-danger';
            case self::CATEGORY_SUCCESS:
                return 'alert-success';
            case self::CATEGORY_INFO:
            default:
                return 'alert-info';
        }
    }

    private function getContent(): string
    {
        $lines = [];
        foreach (\Yii::$app->session->getAllFlashes(true) as $category => $messages) {
            $concatMessage = implode(self::SEPARATOR, $messages);
            if (!empty($concatMessage)) {
                $lines[] = Alert::widget([
                    'options' => ['class' => $this->getClassByCategory($category)],
                    'body'    => $concatMessage,
                ]);
            }
        }

        return implode('', $lines);
    }

    public function run(): string
    {
        return $this->getContent();
    }

}