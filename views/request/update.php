<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $requestForm \app\models\forms\RequestForm */
/* @var $requestId int */

$this->title                   = 'Редактирование заявки: ' . $requestForm->name;
$this->params['breadcrumbs'][] = ['label' => 'Заявки', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $requestForm->name, 'url' => ['view', 'id' => $requestId]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>

<div class="request-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($requestForm, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($requestForm, 'description')->textarea(['rows' => 6]) ?>

    <?php if ($requestForm->scenario === $requestForm::SCENARIO_UPDATE_WITH_RESULT) {
        echo $form->field($requestForm, 'result')->textarea(['rows' => 6]);
    } ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
