<?php

use app\models\rbac\RbacManager;
use app\models\Role;
use kartik\daterange\DateRangePicker;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel \app\models\forms\RequestSearchForm */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $statuses array */
/* @var $isAvailableMyUncompletedFilter bool */

$this->title                   = 'Заявки';
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="request-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать заявку', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <p>
        <?php if ($isAvailableMyUncompletedFilter) {
            echo Html::a('Все', ['index', 'my_uncompleted' => null],
                ['class' => 'btn ' . (!empty($searchModel->my_uncompleted) ? '' : 'active')]);
            echo Html::a('Мои незавершенные', ['index', 'my_uncompleted' => 1],
                ['class' => 'btn ' . (!empty($searchModel->my_uncompleted) ? 'active' : '')]);
        } ?>
    </p>

    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',
            [
                'attribute' => 'manager_name',
                'value'     => 'manager.name',
            ],
            [
                'attribute' => 'creator_name',
                'value'     => 'creator.name',
            ],

            [
                'attribute' => 'date_create',
                'format'    => ['date', 'php:d.m.Y H:i'],
                'filter'    => DateRangePicker::widget([
                    'model'         => $searchModel,
                    'attribute'     => 'date_create_filter',
                    'convertFormat' => true,
                    'pluginOptions' => ['locale' => ['format' => 'd.m.Y']],
                ]),
            ],

            [
                'attribute' => 'status_id',
                'value'     => 'status.name',
                'label'     => $searchModel->getAttributeLabel('status_name'),
                'filter'    => $statuses,
            ],

            [
                'class'    => 'yii\grid\ActionColumn',
                'template' => '{view}',
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>

</div>
