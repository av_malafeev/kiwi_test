<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $requestForm \app\models\forms\RequestForm */
/* @var $requestId int */

$this->title                   = 'Отправить на проверку: ' . $requestForm->name;
$this->params['breadcrumbs'][] = ['label' => 'Заявки', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $requestForm->name, 'url' => ['view', 'id' => $requestId]];
$this->params['breadcrumbs'][] = 'Оправить на проверку';
?>

<div class="request-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($requestForm, 'result')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Отправить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
