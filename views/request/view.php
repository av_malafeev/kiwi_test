<?php

use app\models\Action;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $requestViewForm \app\models\forms\RequestViewForm */
/* @var $availableActions array */
/* @var $statuses array */
/* @var $changeStatusData array */

$this->title                   = $requestViewForm->name;
$this->params['breadcrumbs'][] = ['label' => 'Заявки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="request-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_actions', ['requestId' => $requestViewForm->id, 'availableActions' => $availableActions]); ?>

    <?= DetailView::widget([
        'model'      => $requestViewForm,
        'attributes' => [
            'creator',
            'manager',
            [
                'attribute' => 'dateCreate',
                'format'    => ['date', 'php:d.m.Y H:i'],
            ],
            [
                'attribute' => 'status',
                'value'     => Html::encode($statuses[$requestViewForm->status] ?? ''),
            ],
            'name',
            'description:ntext',
            'result:ntext',
        ],
    ]) ?>


    <?php if ($availableActions[Action::READ_CHANGE_STATUS_LOG]) {
        echo $this->render('_change_status_log', ['changeStatusData' => $changeStatusData, 'statuses' => $statuses]);
    } ?>
</div>
