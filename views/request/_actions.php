<?php

use app\models\Action;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $requestId int */
/* @var $availableActions array */
?>

<p>
    <?php if (!empty($availableActions[Action::UPDATE])) {
        echo Html::a('Отредактировать', ['update', 'id' => $requestId], ['class' => 'btn btn-primary']);
    } ?>

    <?php if (!empty($availableActions[Action::DELETE])) {
        echo Html::a('Удалить', ['delete', 'id' => $requestId], [
            'class' => 'btn btn-danger',
            'data'  => [
                'confirm' => 'Удалить заявку?',
                'method'  => 'post',
            ],
        ]);
    } ?>

    <?php if (!empty($availableActions[Action::START_EXECUTION])) {
        echo Html::a('Начать выполнение', ['start-execution', 'id' => $requestId], [
            'class' => 'btn btn-primary',
            'data'  => ['method' => 'post'],
        ]);
    } ?>

    <?php if (!empty($availableActions[Action::SEND_FOR_REVIEW])) {
        echo Html::a('Отправить на проверку', ['send-for-review', 'id' => $requestId], [
            'class' => 'btn btn-primary',
        ]);
    } ?>

    <?php if (!empty($availableActions[Action::CLOSE])) {
        echo Html::a('Закрыть', ['close', 'id' => $requestId], [
            'class' => 'btn btn-primary',
            'data'  => ['method' => 'post'],
        ]);
    } ?>
</p>