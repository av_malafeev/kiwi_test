<?php

use app\models\ChangeStatusLog;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $changeStatusData array */
/* @var $item ChangeStatusLog */
/* @var $statuses array */

?>

<h2>Журнал изменения статусов</h2>

<table class="table table-striped table-bordered">
    <thead>
    <tr>
        <th>Пользователь</th>
        <th>Дата изменения</th>
        <th>Статус</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($changeStatusData as $item): ?>
        <tr>
            <td><?= Html::encode($item->getUser()->getName()) ?></td>
            <td><?= Html::encode(date('d.m.Y H:i', $item->getDataChange())) ?></td>
            <td><?= Html::encode($statuses[$item->getStatus()->getStatusId()] ?? '') ?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>