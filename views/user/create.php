<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model \app\models\ar\ArUser */
/* @var $roles array */

$this->title                   = 'Создание пользователя';
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="user-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', compact('model', 'roles')) ?>

</div>
