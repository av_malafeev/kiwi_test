<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel \app\models\forms\UserSearchForm */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $roles array */

$this->title                   = 'Пользователи';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать пользователя', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            ['class' => 'yii\grid\SerialColumn'],

            'email:email',
            'name',
            [
                'attribute' => 'role',
                'value'     => function ($model) use ($roles) {
                    return $roles[$model->role] ?? '';
                },
                'filter'    => $roles,
            ],

            [
                'class' => 'yii\grid\ActionColumn',
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>

</div>
