<?php

use yii\db\Migration;

class m170806_135249_create_table__change_status_log extends Migration
{
    const TABLE_CHANGE_STATUS_LOG = '{{%change_status_log}}';
    const TABLE_REQUEST = '{{%request}}';
    const TABLE_STATUS = '{{%status}}';
    const TABLE_USER = '{{%user}}';

    const CASCADE = 'CASCADE';

    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        $this->createTable(self::TABLE_CHANGE_STATUS_LOG, [
            'id'          => $this->primaryKey(),
            'request_id'  => $this->integer()->notNull(),
            'date_change' => $this->integer()->notNull(),
            'user_id'     => $this->integer()->notNull(),
            'status_id'   => $this->integer()->notNull(),
        ], $tableOptions);

        $this->addForeignKey(
            'fk_change_status_log__request_id',
            self::TABLE_CHANGE_STATUS_LOG,
            'request_id',
            self::TABLE_REQUEST,
            'id',
            self::CASCADE,
            self::CASCADE
        );

        $this->addForeignKey(
            'fk_change_status_log__status_id',
            self::TABLE_CHANGE_STATUS_LOG,
            'status_id',
            self::TABLE_STATUS,
            'id',
            self::CASCADE,
            self::CASCADE
        );

        $this->addForeignKey(
            'fk_change_status_log__user_id',
            self::TABLE_CHANGE_STATUS_LOG,
            'user_id',
            self::TABLE_USER,
            'id',
            self::CASCADE,
            self::CASCADE
        );
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE_CHANGE_STATUS_LOG);
    }

}
