<?php

use yii\db\Migration;

class m170806_122755_create_table__user extends Migration
{
    const TABLE_USER = '{{%user}}';

    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        $this->createTable(self::TABLE_USER, [
            'id'       => $this->primaryKey(),
            'email'    => $this->string(255)->notNull()->unique(),
            'password' => $this->string(255)->notNull(),
            'name'     => $this->string(255)->notNull(),
            'role'     => $this->string(255)->notNull(),
            'auth_key' => $this->string(255),
        ], $tableOptions);
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE_USER);
    }

}
