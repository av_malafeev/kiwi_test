<?php

use yii\db\Migration;

class m170806_131522_create_table__status extends Migration
{
    const TABLE_STATUS = '{{%status}}';
    const STATUSES = [
        [1, 'Создана'],
        [2, 'В работе'],
        [3, 'На проверке'],
        [4, 'Закрыта'],
    ];

    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        $this->createTable(self::TABLE_STATUS, [
            'id'   => $this->primaryKey(),
            'name' => $this->string(),
        ], $tableOptions);

        $this->batchInsert(self::TABLE_STATUS, ['id', 'name'], self::STATUSES);
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE_STATUS);
    }
}
