<?php

use yii\db\Migration;

class m170806_133252_create_table__request extends Migration
{
    const TABLE_REQUEST = '{{%request}}';
    const TABLE_STATUS = '{{%status}}';
    const TABLE_USER = '{{%user}}';

    const RESTRICT = 'RESTRICT';

    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        $this->createTable(self::TABLE_REQUEST, [
            'id'          => $this->primaryKey(),
            'date_create' => $this->integer()->notNull(),
            'date_change' => $this->integer(),
            'status_id'   => $this->integer()->notNull(),
            'name'        => $this->string(255)->notNull(),
            'description' => $this->text(),
            'result'      => $this->text(),
            'creator_id'  => $this->integer()->notNull(),
            'manager_id'  => $this->integer(),
        ], $tableOptions);

        $this->addForeignKey(
            'fk_request__status_id',
            self::TABLE_REQUEST,
            'status_id',
            self::TABLE_STATUS,
            'id',
            self::RESTRICT,
            self::RESTRICT
        );

        $this->addForeignKey(
            'fk_request__creator_id',
            self::TABLE_REQUEST,
            'creator_id',
            self::TABLE_USER,
            'id',
            self::RESTRICT,
            self::RESTRICT
        );

        $this->addForeignKey(
            'fk_request__manager_id',
            self::TABLE_REQUEST,
            'manager_id',
            self::TABLE_USER,
            'id',
            self::RESTRICT,
            self::RESTRICT
        );
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE_REQUEST);
    }

}
