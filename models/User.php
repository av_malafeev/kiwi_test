<?php

declare(strict_types=1);

namespace app\models;

/**
 * Class User
 * @package app\models
 */
class User
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */

    private $name;

    /**
     * @var Role
     */
    private $role;


    public function __construct(int $id, string $name, Role $role)
    {
        $this->name = $name;
        $this->role = $role;
        $this->id   = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getRole(): Role
    {
        return $this->role;
    }
}