<?php

declare(strict_types=1);

namespace app\models\exceptions;

/**
 * Class DeleteRequestException
 * @package app\models\exceptions
 */
class DeleteRequestException extends \DomainException
{

}