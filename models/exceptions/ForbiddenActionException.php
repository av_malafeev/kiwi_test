<?php

declare(strict_types=1);

namespace app\models\exceptions;

/**
 * Class ForbiddenActionException
 * @package app\models\exceptions
 */
class ForbiddenActionException extends \DomainException
{

}