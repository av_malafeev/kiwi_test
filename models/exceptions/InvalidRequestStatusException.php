<?php

declare(strict_types=1);

namespace app\models\exceptions;

/**
 * Class InvalidRequestStatusException
 * @package app\models\exceptions
 */
class InvalidRequestStatusException extends \DomainException
{

}