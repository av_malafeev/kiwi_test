<?php

declare(strict_types=1);

namespace app\models\exceptions;

/**
 * Class InvalidParamException
 * @package app\models\exceptions
 */
class InvalidParamException extends \DomainException
{

}