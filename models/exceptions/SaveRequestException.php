<?php

declare(strict_types=1);

namespace app\models\exceptions;

/**
 * Class SaveRequestException
 * @package app\models\exceptions
 */
class SaveRequestException extends \DomainException
{

}