<?php

declare(strict_types=1);

namespace app\models\exceptions;

/**
 * Class RequestNotFoundException
 * @package app\models\exceptions
 */
class RequestNotFoundException extends \DomainException
{

}