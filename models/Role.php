<?php

declare(strict_types=1);

namespace app\models;

/**
 * Class Role
 * @package app
 */
class Role
{
    const MANAGER = 'MANAGER';
    const SUPERVISOR = 'SUPERVISOR';

    /**
     * @var string
     */
    private $name;

    public function __construct(string $name)
    {
        $this->name = $name;
    }

    public function getName(): string
    {
        return $this->name;
    }

}