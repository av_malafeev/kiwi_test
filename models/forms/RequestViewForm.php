<?php

declare(strict_types=1);

namespace app\models\forms;

use app\models\ar\ArStatus;
use app\models\Request;
use yii\base\Model;

/**
 * Class RequestViewForm
 * @package app\models
 */
class RequestViewForm extends Model
{
    public $id;
    public $creator;
    public $manager;
    public $dateCreate;
    public $status;
    public $name;
    public $description;
    public $result;

    public function __construct(Request $request, array $config = [])
    {
        $manager = $request->getManager();

        $this->id          = $request->getId();
        $this->creator     = $request->getCreator()->getName();
        $this->manager     = $manager !== null ? $manager->getName() : null;
        $this->dateCreate  = $request->getDateCreate();
        $this->status      = $request->getStatus()->getStatusId();
        $this->name        = $request->getName();
        $this->description = $request->getDescription();
        $this->result      = $request->getResult();

        parent::__construct($config);
    }

    public function attributeLabels(): array
    {
        return [
            'creator'     => 'Создатель',
            'manager'     => 'Исполнитель',
            'dateCreate'  => 'Дата создания',
            'status'      => 'Текущий статус',
            'name'        => 'Название',
            'description' => 'Описание',
            'result'      => 'Результат работы',
        ];
    }

}