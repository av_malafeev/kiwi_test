<?php

namespace app\models\forms;

use app\models\ar\ArUser;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * Class UserSearchForm
 * @package app\models\forms
 */
class UserSearchForm extends Model
{
    public $id;
    public $email;
    public $name;
    public $role;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['email', 'name', 'role'], 'safe'],
        ];
    }

    /**
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ArUser::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id'   => $this->id,
            'role' => $this->role,
        ])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}
