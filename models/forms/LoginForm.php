<?php

namespace app\models\forms;

use Yii;
use app\models\ar\ArUser;
use yii\base\Model;

/**
 * Class LoginForm
 * @package app\models\forms
 */
class LoginForm extends Model
{
    public $email;
    public $password;

    private $user;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'password'], 'required'],
            ['email', 'email'],
            ['password', 'validatePassword'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'email'    => 'Эл. почта',
            'password' => 'Пароль',
        ];
    }

    /**
     * @param string $attribute
     * @param array  $params
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Эл. почта или пароль введены неверно!');
            }
        }
    }

    /**
     * @return bool
     */
    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser());
        }

        return false;
    }

    /**
     * @return ArUser
     */
    public function getUser()
    {
        if ($this->user === null) {
            $this->user = ArUser::findByEmail($this->email);
        }

        return $this->user;
    }
}
