<?php

declare(strict_types=1);

namespace app\models\forms;

use app\models\Request;
use yii\base\Model;

/**
 * Class RequestForm
 * @package app\models\forms
 */
class RequestForm extends Model
{
    const SCENARIO_CREATE = 'create';
    const SCENARIO_UPDATE = 'update';
    const SCENARIO_UPDATE_WITH_RESULT = 'updateWithResult';
    const SCENARIO_SEND_FOR_REVIEW = 'sendForReview';

    public $name;
    public $description;
    public $result;

    public function __construct(?Request $request, array $config = [])
    {
        if ($request !== null) {
            $this->name        = $request->getName();
            $this->description = $request->getDescription();
            $this->result      = $request->getResult();
        }

        parent::__construct($config);
    }

    public function rules(): array
    {
        return [
            [['description', 'result'], 'string'],
            [['name'], 'string', 'max' => 255],

            [['name', 'description'], 'required', 'on' => self::SCENARIO_CREATE],
            [['name', 'description'], 'required', 'on' => self::SCENARIO_UPDATE],
            [['name', 'description', 'result'], 'required', 'on' => self::SCENARIO_UPDATE_WITH_RESULT],
            ['result', 'required', 'on' => self::SCENARIO_SEND_FOR_REVIEW],
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'name'        => 'Название',
            'description' => 'Описание',
            'result'      => 'Результат работы',
        ];
    }

}