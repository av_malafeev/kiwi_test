<?php

namespace app\models\forms;

use app\models\ar\ArRequest;
use app\models\ar\ArRequestQuery;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * Class RequestSearchForm
 * @package app\models\forms
 */
class RequestSearchForm extends Model
{
    public $name;
    public $status_id;
    public $status_name;
    public $creator_name;
    public $manager_name;
    public $date_create_filter;
    public $my_uncompleted;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'name',
                    'status_id',
                    'status_name',
                    'creator_name',
                    'manager_name',
                    'date_create_filter',
                    'my_uncompleted',
                ],
                'safe',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name'         => 'Название',
            'status_name'  => 'Текущий статус',
            'creator_name' => 'Создатель',
            'manager_name' => 'Исполнитель',
            'date_create'  => 'Дата создания',
        ];
    }

    public function getDateCreateStart()
    {
        $dateInterval = explode(' - ', $this->date_create_filter);

        return isset($dateInterval[0]) ? strtotime($dateInterval[0]) : null;
    }

    public function getDateCreateEnd()
    {
        $dateInterval = explode(' - ', $this->date_create_filter);

        return isset($dateInterval[1]) ? strtotime($dateInterval[1] . ' +1day - 1second') : null;
    }

    public function search($params)
    {
        $query = ArRequest::find()
            ->alias('r')
            ->joinWith(['status as s', 'creator as c', 'manager as m']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['status_id'] = [
            'asc'  => ['s.name' => SORT_ASC],
            'desc' => ['s.name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['manager_name'] = [
            'asc'  => ['m.name' => SORT_ASC],
            'desc' => ['m.name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['creator_name'] = [
            'asc'  => ['c.name' => SORT_ASC],
            'desc' => ['c.name' => SORT_DESC],
        ];

        $this->load($params);
        $this->my_uncompleted = $params['my_uncompleted'] ?? null;

        if (!$this->validate()) {
            return $dataProvider;
        }

        if (!empty($this->my_uncompleted)) {
            /* @var $query ArRequestQuery */
            $query->byManager(\Yii::$app->user->getId())
                ->uncompleted();
        }

        $query->andFilterWhere(['r.status_id' => $this->status_id])
            ->andFilterWhere(['like', 'r.name', $this->name])
            ->andFilterWhere(['like', 'c.name', $this->creator_name])
            ->andFilterWhere(['like', 'm.name', $this->manager_name])
            ->andFilterWhere(['>=', 'r.date_create', $this->getDateCreateStart()])
            ->andFilterWhere(['<=', 'r.date_create', $this->getDateCreateEnd()]);

        return $dataProvider;
    }
}