<?php

declare(strict_types=1);

namespace app\models\repositories;

use app\models\ar\ArChangeStatusLog;
use app\models\ChangeStatusLog;
use app\models\Role;
use app\models\Status;
use app\models\User;

/**
 * Class SqlChangeStatusLogRepository
 * @package app\models\repositories
 */
class SqlChangeStatusLogRepository implements ChangeStatusLogRepository
{
    public function getByRequestId(int $requestId): array
    {
        $models = ArChangeStatusLog::find()->where(['request_id' => $requestId])->all();

        $result = [];
        foreach ($models as $model) {
            $user     = new User((int)$model->user_id, $model->user->name, new Role($model->user->role));
            $status   = new Status($model->status_id);
            $result[] = new ChangeStatusLog($requestId, $user, $status, (int)$model->date_change);
        }

        return $result;
    }

    public function save(ChangeStatusLog $changeStatusLog): void
    {
        $model = new ArChangeStatusLog([
            'request_id'  => $changeStatusLog->getRequestId(),
            'date_change' => $changeStatusLog->getDataChange(),
            'status_id'   => $changeStatusLog->getStatus()->getStatusId(),
            'user_id'     => $changeStatusLog->getUser()->getId(),
        ]);

        $model->save();
    }
}