<?php

declare(strict_types=1);

namespace app\models\repositories;

use app\models\ar\ArRequest;
use app\models\exceptions\DeleteRequestException;
use app\models\exceptions\RequestNotFoundException;
use app\models\exceptions\SaveRequestException;
use app\models\Request;
use app\models\Role;
use app\models\Status;
use app\models\User;

/**
 * Class SqlRequestRepository
 * @package app\models\repositories
 */
class SqlRequestRepository implements RequestRepository
{
    private function getRequestFromDb($id): ArRequest
    {
        $request = ArRequest::findOne($id);
        if ($request === null) {
            throw new RequestNotFoundException("Request not found. id={$id}");
        }

        return $request;
    }

    public function getById(int $id): Request
    {
        $model = $this->getRequestFromDb($id);

        $user    = new User((int)$model->creator->id, $model->creator->name, new Role($model->creator->role));
        $status  = new Status((int)$model->status_id);
        $request = new Request((int)$model->id, $user, $model->name, $model->description, $status,
            (int)$model->date_create, (int)$model->date_change);

        if (!empty($model->result)) {
            $request->setResult($model->result);
        }

        if ($model->manager !== null) {
            $request->setManager(new User((int)$model->manager->id, $model->manager->name,
                new Role($model->manager->role)));
        }

        return $request;
    }

    private function insert(Request $request): int
    {
        $manager = $request->getManager();
        $model   = new ArRequest([
            'date_create' => $request->getDateCreate(),
            'creator_id'  => $request->getCreator()->getId(),
            'status_id'   => $request->getStatus()->getStatusId(),
            'name'        => $request->getName(),
            'description' => $request->getDescription(),
            'result'      => $request->getResult(),
            'manager_id'  => isset($manager) ? $manager->getId() : null,
            'date_change' => time(),
        ]);

        if (!$model->save()) {
            throw new SaveRequestException('Save request error. Error=' . implode('; ', $model->getFirstErrors()));
        }

        return $model->id;
    }

    private function update(Request $request): void
    {
        $model = $this->getRequestFromDb($request->getId());

        $manager = $request->getManager();
        $model->setAttributes([
            'date_create' => $request->getDateCreate(),
            'creator_id'  => $request->getCreator()->getId(),
            'status_id'   => $request->getStatus()->getStatusId(),
            'name'        => $request->getName(),
            'description' => $request->getDescription(),
            'result'      => $request->getResult(),
            'manager_id'  => $manager !== null ? $manager->getId() : null,
            'date_change' => time(),
        ]);

        if (!$model->save()) {
            throw new SaveRequestException('Save request error. Error=' . implode('; ', $model->getFirstErrors()));
        }
    }

    public function save(Request $request): int
    {
        if ($request->getId()) {
            $this->update($request);

            return $request->getId();
        } else {
            return $this->insert($request);
        }
    }

    public function delete(Request $request): void
    {
        $model = $this->getRequestFromDb($request->getId());

        if (!$model->delete()) {
            throw new DeleteRequestException('Delete request error. Error=' . implode('; ', $model->getFirstErrors()));
        }
    }
}