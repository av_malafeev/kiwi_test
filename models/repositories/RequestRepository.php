<?php

namespace app\models\repositories;

use app\models\Request;

/**
 * Interface RequestRepository
 * @package app\models\repositories
 */
interface RequestRepository
{
    public function save(Request $request): int;

    public function getById(int $id): Request;

    public function delete(Request $request): void;
}