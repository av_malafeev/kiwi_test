<?php

declare(strict_types=1);

namespace app\models\repositories;

use app\models\ChangeStatusLog;

/**
 * Interface ChangeStatusLogRepository
 * @package app\models\repositories
 */
interface ChangeStatusLogRepository
{
    public function getByRequestId(int $requestId): array;

    public function save(ChangeStatusLog $changeStatusLog): void;
}
