<?php

declare(strict_types=1);

namespace app\models;

/**
 * Class ChangeStatusLog
 * @package app\models
 */
class ChangeStatusLog
{
    /**
     * @var int
     */
    private $requestId;

    /**
     * @var User
     */
    private $user;

    /**
     * @var Status
     */
    private $status;

    /**
     * @var int
     */
    private $dataChange;

    public function __construct(int $requestId, User $user, Status $status, ?int $dataChange = null)
    {
        $this->user       = $user;
        $this->status     = $status;
        $this->dataChange = $dataChange ?? time();
        $this->requestId  = $requestId;
    }

    /**
     * @return int
     */
    public function getRequestId(): int
    {
        return $this->requestId;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @return Status
     */
    public function getStatus(): Status
    {
        return $this->status;
    }

    /**
     * @return int
     */
    public function getDataChange(): int
    {
        return $this->dataChange;
    }
}