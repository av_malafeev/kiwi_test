<?php

declare(strict_types=1);

namespace app\models;

/**
 * Class Action
 * @package app\models
 */
class Action
{
    const CREATE = 'create';
    const UPDATE = 'update';
    const DELETE = 'delete';
    const START_EXECUTION = 'startExecution';
    const SEND_FOR_REVIEW = 'sendForReview';
    const CLOSE = 'close';
    const READ_CHANGE_STATUS_LOG = 'readChangeStatusLog';

    /**
     * @var string
     */
    private $action;

    public function __construct(string $action)
    {
        $this->action = $action;
    }

    public function getAction(): string
    {
        return $this->action;
    }

}