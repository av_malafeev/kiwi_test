<?php

declare(strict_types=1);

namespace app\models;

use app\models\exceptions\ForbiddenActionException;
use app\models\exceptions\InvalidParamException;
use app\models\exceptions\InvalidRequestStatusException;
use app\models\repositories\ChangeStatusLogRepository;
use app\models\repositories\RequestRepository;

/**
 * Class RequestService
 * @package app\models
 */
class RequestService
{
    /**
     * @var RequestRepository
     */
    private $repository;

    /**
     * @var ChangeStatusLogRepository
     */
    private $changeStatusLogRepository;

    /**
     * @var User
     */
    private $user;

    public function __construct(RequestRepository $repository, ChangeStatusLogRepository $changeStatusLogRepository)
    {
        $this->repository                = $repository;
        $this->changeStatusLogRepository = $changeStatusLogRepository;
    }

    public function setUser(User $user): void
    {
        $this->user = $user;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    private function hasPermissionTo(Action $action): bool
    {
        $permission = new ActionPermission($action);

        return $permission->allowedToRole($this->user->getRole());
    }

    private function checkPermissionToAction(Action $action): void
    {
        if (!$this->hasPermissionTo($action)) {
            throw new ForbiddenActionException("Forbidden execute action: action={$action->getAction()}, user={$this->user->getId()}");
        }
    }

    private function checkStatusChange(Request $request, Status $status): void
    {
        if (!$request->getStatus()->canChangeTo($status)) {
            throw new InvalidRequestStatusException("Invalid request status: current={$request->getStatus()->getStatusId()}, new={$status->getStatusId()}");
        }
    }

    public function create(string $name, string $description): int
    {
        $this->checkPermissionToAction(new Action(Action::CREATE));

        $newStatus = new Status(Status::CREATED);
        $request   = new Request(null, $this->user, $name, $description, $newStatus);

        $requestId = $this->repository->save($request);
        $this->changeStatusLogRepository->save(new ChangeStatusLog($requestId, $this->user, $newStatus));

        return $requestId;
    }

    public function update(Request $request, string $name, string $description, ?string $result): void
    {
        $this->checkPermissionToAction(new Action(Action::UPDATE));

        $request->setName($name);
        $request->setDescription($description);

        if ($result !== null) {
            if (!$request->isAllowedUpdateResult()) {
                throw new InvalidParamException("Forbidden to update result: status={$request->getStatus()->getStatusId()}, result={$result}");
            }
            $request->setResult($result);
        }

        $this->repository->save($request);
    }

    public function delete(Request $request): void
    {
        $this->checkPermissionToAction(new Action(Action::DELETE));

        $this->repository->delete($request);
    }

    public function startExecution(Request $request): void
    {
        $this->checkPermissionToAction(new Action(Action::START_EXECUTION));

        $newStatus = new Status(Status::IN_THE_WORK);
        $this->checkStatusChange($request, $newStatus);

        $request->setManager($this->user);
        $request->setStatus($newStatus);

        $this->repository->save($request);
        $this->changeStatusLogRepository->save(new ChangeStatusLog($request->getId(), $this->user, $newStatus));
    }

    public function sendForReview(Request $request, string $result): void
    {
        $action = new Action(Action::SEND_FOR_REVIEW);
        $this->checkPermissionToAction($action);

        $newStatus = new Status(Status::ON_REVIEW);
        $this->checkStatusChange($request, $newStatus);

        if (!$request->isManager($this->user)) {
            throw new ForbiddenActionException("Allowed to send for review only own requests: user={$this->user->getId()}, manager={$request->getManager()->getId()}");
        }

        $request->setResult($result);
        $request->setStatus($newStatus);

        $this->repository->save($request);
        $this->changeStatusLogRepository->save(new ChangeStatusLog($request->getId(), $this->user, $newStatus));
    }

    public function close(Request $request): void
    {
        $this->checkPermissionToAction(new Action(Action::CLOSE));

        $newStatus = new Status(Status::CLOSED);
        $this->checkStatusChange($request, $newStatus);

        $request->setStatus($newStatus);

        $this->repository->save($request);
        $this->changeStatusLogRepository->save(new ChangeStatusLog($request->getId(), $this->user, $newStatus));
    }

    public function canCreate(): bool
    {
        return $this->hasPermissionTo(new Action(Action::CREATE));
    }

    public function canUpdate(): bool
    {
        return $this->hasPermissionTo(new Action(Action::UPDATE));
    }

    public function canDelete(): bool
    {
        return $this->hasPermissionTo(new Action(Action::DELETE));
    }

    public function canStartExecution(Request $request): bool
    {
        return $this->hasPermissionTo(new Action(Action::START_EXECUTION))
            && $request->getStatus()->canChangeTo(new Status(Status::IN_THE_WORK));
    }

    public function canSendForReview(Request $request): bool
    {
        return $this->hasPermissionTo(new Action(Action::SEND_FOR_REVIEW))
            && $request->getStatus()->canChangeTo(new Status(Status::ON_REVIEW))
            && $request->isManager($this->user);
    }

    public function canClose(Request $request): bool
    {
        return $this->hasPermissionTo(new Action(Action::CLOSE))
            && $request->getStatus()->canChangeTo(new Status(Status::CLOSED));
    }

    public function canReadChangeStatusLog()
    {
        return $this->hasPermissionTo(new Action(Action::READ_CHANGE_STATUS_LOG));
    }
}
