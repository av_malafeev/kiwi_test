<?php

declare(strict_types=1);

namespace app\models;

use app\models\ar\ArStatus;

/**
 * Class StatusService
 * @package app\models
 */
class StatusService
{
    public function getStatuses(): array
    {
        return ArStatus::getStatusList();
    }
}