<?php

declare(strict_types=1);

namespace app\models;

/**
 * Class ActionPermission
 * @package app\models
 */
class ActionPermission
{
    /**
     * @var Action
     */
    private $action;

    public function __construct(Action $action)
    {
        $this->action = $action;
    }

    public function allowedToRole(Role $role): bool
    {
        switch ($role->getName()) {
            case Role::MANAGER:
                return in_array($this->action->getAction(),
                    [Action::CREATE, Action::START_EXECUTION, Action::SEND_FOR_REVIEW], true);
            case Role::SUPERVISOR:
                return in_array($this->action->getAction(),
                    [Action::CREATE, Action::UPDATE, Action::DELETE, Action::CLOSE, Action::READ_CHANGE_STATUS_LOG],
                    true);
            default:
                return false;
        }
    }
}