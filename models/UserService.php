<?php

declare(strict_types=1);

namespace app\models;

/**
 * Class UserService
 * @package app\models
 */
class UserService
{
    public function getRoles(): array
    {
        return [
            Role::MANAGER    => 'Менеджер',
            Role::SUPERVISOR => 'Супервайзер',
        ];
    }
}