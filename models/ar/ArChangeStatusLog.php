<?php

namespace app\models\ar;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "change_status_log".
 *
 * @property integer   $id
 * @property integer   $request_id
 * @property integer   $date_change
 * @property integer   $user_id
 * @property integer   $status_id
 *
 * @property ArRequest $request
 * @property ArStatus  $status
 * @property ArUser    $user
 */
class ArChangeStatusLog extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'change_status_log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['request_id', 'date_change', 'user_id', 'status_id'], 'required'],
            [['request_id', 'date_change', 'user_id', 'status_id'], 'integer'],
            [
                ['request_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => ArRequest::className(),
                'targetAttribute' => ['request_id' => 'id'],
            ],
            [
                ['status_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => ArStatus::className(),
                'targetAttribute' => ['status_id' => 'id'],
            ],
            [
                ['user_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => ArUser::className(),
                'targetAttribute' => ['user_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => 'ID',
            'request_id'  => 'Request ID',
            'date_change' => 'Date Change',
            'user_id'     => 'User ID',
            'status_id'   => 'Status ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequest()
    {
        return $this->hasOne(ArRequest::className(), ['id' => 'request_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(ArStatus::className(), ['id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(ArUser::className(), ['id' => 'user_id']);
    }
}
