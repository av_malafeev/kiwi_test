<?php

namespace app\models\ar;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "request".
 *
 * @property integer             $id
 * @property integer             $date_create
 * @property integer             $date_change
 * @property integer             $status_id
 * @property string              $name
 * @property string              $description
 * @property string              $result
 * @property integer             $creator_id
 * @property integer             $manager_id
 *
 * @property ArChangeStatusLog[] $changeStatusLogs
 * @property ArUser              $creator
 * @property ArUser              $manager
 * @property ArStatus            $status
 */
class ArRequest extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'request';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date_create', 'date_change', 'status_id', 'creator_id', 'manager_id'], 'integer'],
            [['description', 'result'], 'string'],
            [['name'], 'string', 'max' => 255],
            [
                ['creator_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => ArUser::className(),
                'targetAttribute' => ['creator_id' => 'id'],
            ],
            [
                ['manager_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => ArUser::className(),
                'targetAttribute' => ['manager_id' => 'id'],
            ],
            [
                ['status_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => ArStatus::className(),
                'targetAttribute' => ['status_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'           => 'ID',
            'date_create'  => 'Дата создания',
            'status_id'    => 'Status ID',
            'name'         => 'Название',
            'description'  => 'Описание',
            'result'       => 'Результат работы',
            'creator_id'   => 'Creator ID',
            'manager_id'   => 'Manager ID',
            'status_name'  => 'Текущий статус',
            'creator_name' => 'Создатель',
            'manager_name' => 'Исполнитель',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChangeStatusLogs()
    {
        return $this->hasMany(ArChangeStatusLog::className(), ['request_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreator()
    {
        return $this->hasOne(ArUser::className(), ['id' => 'creator_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManager()
    {
        return $this->hasOne(ArUser::className(), ['id' => 'manager_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(ArStatus::className(), ['id' => 'status_id']);
    }

    /**
     * @return ArRequestQuery
     */
    public static function find()
    {
        return new ArRequestQuery(get_called_class());
    }

}
