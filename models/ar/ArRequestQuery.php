<?php

declare(strict_types=1);

namespace app\models\ar;

use app\models\Status;
use yii\db\ActiveQuery;

/**
 * Class RequestQuery
 * @package app\models\ar
 */
class ArRequestQuery extends ActiveQuery
{
    public function byId(int $id): ArRequestQuery
    {
        return $this->andWhere(['id' => $id]);
    }

    /**
     * @param int $managerId
     *
     * @return ArRequestQuery
     */
    public function byManager(int $managerId): ArRequestQuery
    {
        return $this->andWhere(['manager_id' => $managerId]);
    }

    /**
     * @return ArRequestQuery
     */
    public function uncompleted(): ArRequestQuery
    {
        return $this->andWhere(['status_id' => [Status::CREATED, Status::IN_THE_WORK, Status::ON_REVIEW]]);
    }

    /**
     * @inheritdoc
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
