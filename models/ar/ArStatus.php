<?php

namespace app\models\ar;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "status".
 *
 * @property integer             $id
 * @property string              $name
 *
 * @property ArChangeStatusLog[] $changeStatusLogs
 * @property ArRequest[]         $requests
 */
class ArStatus extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'   => 'ID',
            'name' => 'Статус',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChangeStatusLogs()
    {
        return $this->hasMany(ArChangeStatusLog::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequests()
    {
        return $this->hasMany(ArRequest::className(), ['status_id' => 'id']);
    }

    /**
     * @return array
     */
    public static function getStatusList()
    {
        return ArrayHelper::map(self::find()->all(), 'id', 'name');
    }
}
