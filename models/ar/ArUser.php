<?php

namespace app\models\ar;

use yii\base\Exception;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string  $email
 * @property string  $password
 * @property string  $name
 * @property string  $role
 * @property string  $auth_key
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property string  $pure_password
 */
class ArUser extends ActiveRecord implements IdentityInterface
{
    const SCENARIO_CREATE = 'SCENARIO_CREATE_USER';

    public $pure_password;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'name'], 'required'],
            ['email', 'unique'],
            ['email', 'email'],
            [['password', 'name', 'role', 'auth_key'], 'string', 'max' => 255],

            [['pure_password'], 'string', 'min' => 6, 'max' => 255],
            [['pure_password'], 'required', 'on' => self::SCENARIO_CREATE],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'            => 'ID',
            'email'         => 'Эл. почта',
            'password'      => 'Пароль',
            'pure_password' => 'Пароль',
            'name'          => 'Имя',
            'role'          => 'Роль',
        ];
    }

    public function validatePassword($password)
    {
        return YII_ENV_DEV ? $password === '123123' : \Yii::$app->getSecurity()->validatePassword($password,
            $this->password);
    }

    /**
     * @param string $email
     *
     * @return ArUser
     */
    public static function findByEmail($email)
    {
        return self::find()->where(['email' => $email])->one();
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return self::findOne($id);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    private function generateAuthKey()
    {
        $this->auth_key = \Yii::$app->security->generateRandomString();
    }

    private function generatePasswordHash()
    {
        if (!empty($this->pure_password)) {
            $this->password = \Yii::$app->security->generatePasswordHash($this->pure_password);
        }
    }


    /**
     * @inheritdoc
     *
     * @throws Exception
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->generateAuthKey();
            }

            $this->generatePasswordHash();

            return true;
        }

        return false;
    }
}
