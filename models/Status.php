<?php

declare(strict_types=1);

namespace app\models;

/**
 * Class Status
 * @package app\models
 */
class Status
{
    const CREATED = 1;
    const IN_THE_WORK = 2;
    const ON_REVIEW = 3;
    const CLOSED = 4;

    /**
     * @var int
     */
    private $statusId;

    public function __construct(int $statusId)
    {
        $this->statusId = $statusId;
    }

    public function getStatusId(): int
    {
        return $this->statusId;
    }

    public function canChangeTo(Status $status): bool
    {
        switch ($this->statusId) {
            case self::CREATED:
                return $status->getStatusId() === self::IN_THE_WORK;
            case self::IN_THE_WORK:
                return $status->getStatusId() === self::ON_REVIEW;
            case self::ON_REVIEW:
                return $status->getStatusId() === self::CLOSED;
            default:
                return false;
        }
    }

}