<?php

declare(strict_types=1);

namespace app\models;

/**
 * Class Request
 * @package app\models
 */
class Request
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $dateCreate;

    /**
     * @var int
     */
    private $dateChange;

    /**
     * @var User
     */
    private $creator;

    /**
     * @var User
     */
    private $manager;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $result;

    /**
     * @var Status
     */
    private $status;

    public function __construct(
        ?int $id,
        User $creator,
        string $name,
        string $description,
        Status $status,
        ?int $dateCreate = null,
        ?int $dateChange = null
    ) {
        $this->id          = $id;
        $this->creator     = $creator;
        $this->name        = $name;
        $this->description = $description;
        $this->status      = $status;
        $this->dateCreate  = $dateCreate ?? time();
        $this->dateChange  = $dateChange;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDateCreate(): int
    {
        return $this->dateCreate;
    }

    public function getDateChange(): int
    {
        return $this->dateChange;
    }

    public function getCreator(): User
    {
        return $this->creator;
    }

    public function getManager(): ?User
    {
        return $this->manager;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getResult(): ?string
    {
        return $this->result;
    }

    public function getStatus(): Status
    {
        return $this->status;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    public function setResult(?string $result): void
    {
        $this->result = $result;
    }

    public function setManager(User $manager): void
    {
        $this->manager = $manager;
    }

    public function setStatus(Status $status)
    {
        $this->status = $status;
    }


    public function isManager(User $manager)
    {
        return $this->manager !== null && $this->manager->getId() === $manager->getId();
    }

    public function isAllowedUpdateResult(): bool
    {
        return in_array($this->status->getStatusId(), [Status::ON_REVIEW, Status::CLOSED], true);
    }
}