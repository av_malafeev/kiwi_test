<?php

return [
    'id'         => 'basic-tests',
    'basePath'   => dirname(__DIR__),
    'language'   => 'en-US',
    'components' => [
        'db'           => require __DIR__ . '/test_db.php',
        'mailer'       => [
            'useFileTransport' => true,
        ],
        'assetManager' => [
            'basePath' => __DIR__ . '/../web/assets',
        ],
        'urlManager'   => [
            'showScriptName' => true,
        ],
        'user'         => [
            'identityClass' => 'app\models\ar\User',
        ],
        'request'      => [
            'cookieValidationKey'  => 'test',
            'enableCsrfValidation' => false,
            // but if you absolutely need it set cookie domain to localhost
            /*
            'csrfCookie' => [
                'domain' => 'localhost',
            ],
            */
        ],
    ],
    'container'  => [
        'definitions' => [
            'app\models\repositories\RequestRepository'         => 'app\models\repositories\SqlRequestRepository',
            'app\models\repositories\ChangeStatusLogRepository' => 'app\models\repositories\SqlChangeStatusLogRepository',
        ],
    ],
    'params'     => require __DIR__ . '/params.php',
];
