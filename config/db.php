<?php

return [
    'class'    => 'yii\db\Connection',
    'dsn'      => 'mysql:host=localhost;dbname=db',
    'username' => 'root',
    'password' => 'password',
    'charset'  => 'utf8',
];
