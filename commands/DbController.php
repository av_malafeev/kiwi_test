<?php

namespace app\commands;

use app\models\ar\ArChangeStatusLog;
use app\models\ar\ArRequest;
use app\models\ar\ArUser;
use app\models\Role;
use app\models\Status;
use Faker\Factory;
use Faker\Generator;
use yii\console\Controller;

/**
 * Class DbController
 * @package app\commands
 */
class DbController extends Controller
{
    const COUNT_USER = 15;
    const COUNT_REQUEST = 32;
    const COUNT_START_EXECUTION_REQUEST = 24;
    const COUNT_SEND_TO_REVIEW_REQUEST = 16;
    const COUNT_CLOSE_REQUEST = 8;

    /**
     * @var Generator
     */
    private $faker;

    /**
     * @param ArRequest $request
     * @param int       $userId
     */
    private function saveChangeStatusLog($request, $userId)
    {
        $changeStatusLog = new ArChangeStatusLog([
            'request_id'  => $request->id,
            'date_change' => $request->date_change,
            'status_id'   => $request->status_id,
            'user_id'     => $userId,
        ]);
        $changeStatusLog->save();
    }

    /**
     * @return ArUser
     */
    private function generateUser()
    {
        $user = new ArUser([
            'scenario'      => ArUser::SCENARIO_CREATE,
            'name'          => $this->faker->name,
            'pure_password' => $this->faker->password(),
            'email'         => $this->faker->email,
            'role'          => $this->faker->randomElement([Role::SUPERVISOR, Role::MANAGER]),
        ]);
        $user->save();

        return $user;
    }

    /**
     * @param array $managerIds
     *
     * @return ArRequest
     */
    private function generateRequest($managerIds)
    {
        $request = new ArRequest([
            'creator_id'  => $this->faker->randomElement($managerIds),
            'date_create' => time() - random_int(0, 400000),
            'date_change' => time(),
            'status_id'   => Status::CREATED,
            'name'        => $this->faker->paragraph(1, false),
            'description' => $this->faker->paragraph(),
        ]);
        $request->save();
        $this->saveChangeStatusLog($request, $request->creator_id);

        return $request;
    }

    /**
     * @param ArRequest $request
     * @param array     $managerIds
     *
     * @return ArRequest
     */
    private function startExecution($request, $managerIds)
    {
        $request->refresh();

        $request->date_change = time();
        $request->status_id   = Status::IN_THE_WORK;
        $request->manager_id  = $this->faker->randomElement($managerIds);

        $request->save();
        $this->saveChangeStatusLog($request, $request->manager_id);

        return $request;
    }

    /**
     * @param ArRequest $request
     *
     * @return ArRequest
     */
    private function sendToReview($request)
    {
        $request->refresh();

        $request->date_change = time();
        $request->status_id   = Status::ON_REVIEW;
        $request->result      = $this->faker->paragraph(1, false);

        $request->save();
        $this->saveChangeStatusLog($request, $request->manager_id);

        return $request;
    }

    /**
     * @param ArRequest $request
     * @param array     $supervisorIds
     *
     * @return ArRequest
     */
    private function close($request, $supervisorIds)
    {
        $request->refresh();

        $request->date_change = time();
        $request->status_id   = Status::CLOSED;

        $request->save();
        $this->saveChangeStatusLog($request, $this->faker->randomElement($supervisorIds));

        return $request;
    }

    public function actionGenerate()
    {
        $this->faker = Factory::create();

        for ($i = 0; $i < self::COUNT_USER; $i++) {
            $this->generateUser();
        }

        $managerIds    = ArUser::find()->where(['role' => Role::MANAGER])->select('id')->column();
        $supervisorIds = ArUser::find()->where(['role' => Role::SUPERVISOR])->select('id')->column();

        for ($i = 0; $i < self::COUNT_REQUEST; $i++) {
            $this->generateRequest($managerIds);
        }

        $requests = ArRequest::find()->all();
        shuffle($requests);
        $startExecutionRequests = array_slice($requests, 0, self::COUNT_START_EXECUTION_REQUEST);
        foreach ($startExecutionRequests as $request) {
            $this->startExecution($request, $managerIds);
        }

        shuffle($startExecutionRequests);
        $sendToReviewRequests = array_slice($startExecutionRequests, 0, self::COUNT_SEND_TO_REVIEW_REQUEST);
        foreach ($sendToReviewRequests as $request) {
            $this->sendToReview($request);
        }

        shuffle($sendToReviewRequests);
        $closeRequests = array_slice($startExecutionRequests, 0, self::COUNT_CLOSE_REQUEST);
        foreach ($closeRequests as $request) {
            $this->close($request, $supervisorIds);
        }
    }
}
