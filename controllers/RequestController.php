<?php

namespace app\controllers;

use Yii;
use yii\base\Module;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\Response;
use app\models\Action;
use app\models\ar\ArUser;
use app\models\exceptions\ForbiddenActionException;
use app\models\exceptions\RequestNotFoundException;
use app\models\forms\RequestForm;
use app\models\forms\RequestSearchForm;
use app\models\forms\RequestViewForm;
use app\models\repositories\ChangeStatusLogRepository;
use app\models\repositories\RequestRepository;
use app\models\Request;
use app\models\RequestService;
use app\models\Role;
use app\models\StatusService;
use app\models\User;
use app\widgets\FlashMessage;


/**
 * Class RequestController
 * @package app\controllers
 */
class RequestController extends Controller
{
    const MESSAGE_REQUEST_NOT_FOUND = 'Заявка не найдена';
    const MESSAGE_FORBIDDEN_ACTION = 'У Вас нет недостаточно прав';
    const MESSAGE_CREATE_ERROR = 'Возникла ошибка при создании заявки';
    const MESSAGE_UPDATE_ERROR = 'Возникла ошибка при редактировании заявки';
    const MESSAGE_DELETE_ERROR = 'Возникла ошибка при удалении заявки';
    const MESSAGE_SEND_FOR_REVIEW_ERROR = 'Возникла ошибка при передачи заявки на проверку';
    const MESSAGE_START_EXECUTION_ERROR = 'Возникла ошибка при взятии заявки в работу';
    const MESSAGE_CLOSE_ERROR = 'Возникла ошибка при закрытии заявки';

    /**
     * @var RequestRepository
     */
    private $requestRepository;

    /**
     * @var ChangeStatusLogRepository
     */
    private $changeStatusRepository;

    /**
     * @var RequestService
     */
    private $requestService;

    /**
     * @var StatusService
     */
    private $statusService;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete'          => ['post'],
                    'start-execution' => ['post'],
                    'close'           => ['post'],
                ],
            ],
        ];
    }

    public function __construct(
        $id,
        Module $module,
        RequestRepository $requestRepository,
        ChangeStatusLogRepository $changeStatusRepository,
        RequestService $requestService,
        StatusService $statusService,
        array $config = []
    ) {
        parent::__construct($id, $module, $config);

        $this->requestRepository      = $requestRepository;
        $this->changeStatusRepository = $changeStatusRepository;
        $this->requestService         = $requestService;
        $this->statusService          = $statusService;

        if (!Yii::$app->user->isGuest) {
            /* @var $identity ArUser */
            $identity = Yii::$app->user->identity;

            $this->requestService->setUser(new User($identity->id, $identity->name, new Role($identity->role)));
        }
    }

    private function setErrorMessageAndRedirect(string $message, array $url): Response
    {
        Yii::$app->session->addFlash(FlashMessage::CATEGORY_ERROR, $message);

        return $this->redirect($url);
    }


    public function actionIndex()
    {
        $searchModel  = new RequestSearchForm();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $statuses     = $this->statusService->getStatuses();

        $isAvailableMyUncompletedFilter = $this->requestService->getUser()->getRole()->getName() === Role::MANAGER;

        return $this->render('index',
            compact('searchModel', 'dataProvider', 'statuses', 'isAvailableMyUncompletedFilter'));
    }


    public function actionCreate()
    {
        $requestForm = new RequestForm(null, ['scenario' => RequestForm::SCENARIO_CREATE]);

        if ($requestForm->load(Yii::$app->request->post()) && $requestForm->validate()) {
            try {
                $this->requestService->create($requestForm->name, $requestForm->description);

                return $this->redirect(['index']);
            } catch (ForbiddenActionException $e) {
                return $this->setErrorMessageAndRedirect(self::MESSAGE_FORBIDDEN_ACTION, ['index']);
            } catch (\Exception $e) {
                Yii::error("Create request error. Error={$e->getMessage()}");

                return $this->setErrorMessageAndRedirect(self::MESSAGE_CREATE_ERROR, ['index']);
            }
        }

        return $this->render('create', ['requestForm' => $requestForm]);
    }


    public function actionUpdate($id)
    {
        try {
            $request = $this->requestRepository->getById($id);
        } catch (RequestNotFoundException $e) {
            return $this->setErrorMessageAndRedirect(self::MESSAGE_REQUEST_NOT_FOUND, ['index']);
        }

        $requestForm = new RequestForm($request, [
            'scenario' => $request->isAllowedUpdateResult()
                ? RequestForm::SCENARIO_UPDATE_WITH_RESULT : RequestForm::SCENARIO_UPDATE,
        ]);

        if ($requestForm->load(Yii::$app->request->post()) && $requestForm->validate()) {
            try {
                $this->requestService->update($request, $requestForm->name, $requestForm->description,
                    $requestForm->result);

                return $this->redirect(['view', 'id' => $id]);
            } catch (ForbiddenActionException $e) {
                return $this->setErrorMessageAndRedirect(self::MESSAGE_FORBIDDEN_ACTION, ['view', 'id' => $id]);
            } catch (\Exception $e) {
                Yii::error("Update request error. Error={$e->getMessage()}");

                return $this->setErrorMessageAndRedirect(self::MESSAGE_UPDATE_ERROR, ['view', 'id' => $id]);
            }
        }

        return $this->render('update', ['requestId' => $id, 'requestForm' => $requestForm]);
    }


    public function actionDelete($id)
    {
        try {
            $request = $this->requestRepository->getById($id);
            $this->requestService->delete($request);

            return $this->redirect(['index']);
        } catch (RequestNotFoundException $e) {
            return $this->setErrorMessageAndRedirect(self::MESSAGE_REQUEST_NOT_FOUND, ['index']);
        } catch (ForbiddenActionException $e) {
            return $this->setErrorMessageAndRedirect(self::MESSAGE_FORBIDDEN_ACTION, ['view', 'id' => $id]);
        } catch (\Exception $e) {
            Yii::error("Delete request error. Error={$e->getMessage()}");

            return $this->setErrorMessageAndRedirect(self::MESSAGE_DELETE_ERROR, ['view', 'id' => $id]);
        }
    }


    public function actionStartExecution($id)
    {
        try {
            $request = $this->requestRepository->getById($id);
            $this->requestService->startExecution($request);

            return $this->redirect(['view', 'id' => $id]);
        } catch (RequestNotFoundException $e) {
            return $this->setErrorMessageAndRedirect(self::MESSAGE_REQUEST_NOT_FOUND, ['index']);
        } catch (ForbiddenActionException $e) {
            return $this->setErrorMessageAndRedirect(self::MESSAGE_FORBIDDEN_ACTION, ['view', 'id' => $id]);
        } catch (\Exception $e) {
            Yii::error("Start execution error. Error={$e->getMessage()}");

            return $this->setErrorMessageAndRedirect(self::MESSAGE_START_EXECUTION_ERROR, ['view', 'id' => $id]);
        }
    }


    public function actionSendForReview($id)
    {
        try {
            $request = $this->requestRepository->getById($id);
        } catch (RequestNotFoundException $e) {
            return $this->setErrorMessageAndRedirect(self::MESSAGE_REQUEST_NOT_FOUND, ['index']);
        }

        $requestForm = new RequestForm($request, ['scenario' => RequestForm::SCENARIO_SEND_FOR_REVIEW]);

        if ($requestForm->load(Yii::$app->request->post()) && $requestForm->validate()) {
            try {
                $this->requestService->sendForReview($request, $requestForm->result);

                return $this->redirect(['view', 'id' => $id]);
            } catch (ForbiddenActionException $e) {
                return $this->setErrorMessageAndRedirect(self::MESSAGE_FORBIDDEN_ACTION, ['view', 'id' => $id]);
            } catch (\Exception $e) {
                Yii::error("Send request for review error. Error={$e->getMessage()}");

                return $this->setErrorMessageAndRedirect(self::MESSAGE_SEND_FOR_REVIEW_ERROR, ['view', 'id' => $id]);
            }
        }

        return $this->render('review', ['requestId' => $id, 'requestForm' => $requestForm]);
    }


    public function actionClose($id)
    {
        try {
            $request = $this->requestRepository->getById($id);
            $this->requestService->close($request);

            return $this->redirect(['view', 'id' => $id]);
        } catch (RequestNotFoundException $e) {
            return $this->setErrorMessageAndRedirect(self::MESSAGE_REQUEST_NOT_FOUND, ['index']);
        } catch (ForbiddenActionException $e) {
            return $this->setErrorMessageAndRedirect(self::MESSAGE_FORBIDDEN_ACTION, ['view', 'id' => $id]);
        } catch (\Exception $e) {
            Yii::error("Close request error. Error={$e->getMessage()}");

            return $this->setErrorMessageAndRedirect(self::MESSAGE_CLOSE_ERROR, ['view', 'id' => $id]);
        }
    }


    private function getAvailableActions(Request $request)
    {
        return [
            Action::CREATE                 => $this->requestService->canCreate(),
            Action::UPDATE                 => $this->requestService->canUpdate(),
            Action::DELETE                 => $this->requestService->canDelete(),
            Action::START_EXECUTION        => $this->requestService->canStartExecution($request),
            Action::SEND_FOR_REVIEW        => $this->requestService->canSendForReview($request),
            Action::CLOSE                  => $this->requestService->canClose($request),
            Action::READ_CHANGE_STATUS_LOG => $this->requestService->canReadChangeStatusLog(),
        ];
    }

    public function actionView($id)
    {
        try {
            $request          = $this->requestRepository->getById($id);
            $availableActions = $this->getAvailableActions($request);
            $statuses         = $this->statusService->getStatuses();
            $changeStatusData = $this->changeStatusRepository->getByRequestId($id);

            $requestViewForm = new RequestViewForm($request);

            return $this->render('view', [
                'requestViewForm'  => $requestViewForm,
                'availableActions' => $availableActions,
                'statuses'         => $statuses,
                'changeStatusData' => $changeStatusData,
            ]);
        } catch (RequestNotFoundException $e) {
            return $this->setErrorMessageAndRedirect(self::MESSAGE_REQUEST_NOT_FOUND, ['index']);
        }
    }

}