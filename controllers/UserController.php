<?php

namespace app\controllers;

use app\models\ar\ArUser;
use app\models\forms\UserSearchForm;
use app\models\UserService;
use app\widgets\FlashMessage;
use Yii;
use yii\base\Module;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * Class UserController
 * @package app\controllers
 */
class UserController extends Controller
{
    /**
     * @var UserService
     */
    private $userService;

    public function __construct($id, Module $module, UserService $userService, array $config = [])
    {
        parent::__construct($id, $module, $config);

        $this->userService = $userService;
    }

    private function addErrorToFlashMessage(string $message)
    {
        Yii::$app->session->addFlash(FlashMessage::CATEGORY_ERROR, $message);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel  = new UserSearchForm();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $roles        = $this->userService->getRoles();

        return $this->render('index', compact('searchModel', 'dataProvider', 'roles'));
    }

    /**
     * @param int $id
     *
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $roles = $this->userService->getRoles();

        return $this->render('view', compact('model', 'roles'));
    }

    /**
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ArUser(['scenario' => ArUser::SCENARIO_CREATE]);

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }

            $this->addErrorToFlashMessage("Create user error. Error=" . implode("\n", $model->getFirstErrors()));
        }

        $roles = $this->userService->getRoles();

        return $this->render('create', compact('model', 'roles'));
    }

    /**
     * @param int $id
     *
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
            $this->addErrorToFlashMessage("Update user error. Error=" . implode("\n", $model->getFirstErrors()));
        }

        $roles = $this->userService->getRoles();

        return $this->render('update', compact('model', 'roles'));
    }

    /**
     * @param int $id
     *
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * @param int $id
     *
     * @return mixed
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = ArUser::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
