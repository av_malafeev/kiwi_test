<?php

declare(strict_types=1);

namespace tests\models;

use app\models\exceptions\ForbiddenActionException;
use app\models\exceptions\InvalidRequestStatusException;
use app\models\Request;
use app\models\RequestService;
use app\models\Role;
use app\models\Status;
use app\models\User;
use Codeception\Test\Unit;
use tests\models\mocks\DummyChangeStatusLogRepository;
use tests\models\mocks\DummyRequestRepository;

/**
 * Class RequestServiceSendForReviewTest
 * @package tests\models
 */
class RequestServiceSendForReviewTest extends Unit
{
    public function sendForReviewData(): array
    {
        return [
            [Role::SUPERVISOR, Status::CREATED, 1, ForbiddenActionException::class],
            [Role::MANAGER, Status::CREATED, 1, InvalidRequestStatusException::class],
            [Role::MANAGER, Status::IN_THE_WORK, 1, null],
            [Role::MANAGER, Status::IN_THE_WORK, 2, ForbiddenActionException::class],
            [Role::MANAGER, Status::ON_REVIEW, 1, InvalidRequestStatusException::class],
            [Role::MANAGER, Status::CLOSED, 1, InvalidRequestStatusException::class],
        ];
    }

    /**
     * @dataProvider sendForReviewData
     *
     * @param string      $role
     * @param int         $status
     * @param int|null    $managerId
     * @param null|string $expectedException
     */
    public function testSendForReview(string $role, int $status, ?int $managerId, ?string $expectedException): void
    {
        if ($expectedException !== null) {
            $this->expectException($expectedException);
        }

        $repository             = new DummyRequestRepository();
        $changeStatusRepository = new DummyChangeStatusLogRepository();
        $service                = new RequestService($repository, $changeStatusRepository);
        $service->setUser(new User(1, 'User', new Role($role)));

        $creator = new User(2, 'Creator', new Role(Role::MANAGER));
        $request = new Request(1, $creator, 'name', 'description', new Status($status));
        if ($managerId !== null) {
            $request->setManager(new User($managerId, 'Manager', new Role(Role::MANAGER)));
        }

        $service->sendForReview($request, 'result');
    }

    public function canSendForReviewData(): array
    {
        return [
            [Role::SUPERVISOR, Status::CREATED, 1, false],
            [Role::MANAGER, Status::CREATED, 1, false],
            [Role::MANAGER, Status::IN_THE_WORK, 1, true],
            [Role::MANAGER, Status::IN_THE_WORK, 2, false],
            [Role::MANAGER, Status::ON_REVIEW, 1, false],
            [Role::MANAGER, Status::CLOSED, 1, false],
        ];
    }

    /**
     * @dataProvider canSendForReviewData
     *
     * @param string   $role
     * @param int      $status
     * @param int|null $managerId
     * @param bool     $expected
     */
    public function testCanSendForReview(string $role, int $status, ?int $managerId, bool $expected): void
    {
        $user                   = new User(1, 'User', new Role($role));
        $repository             = new DummyRequestRepository();
        $changeStatusRepository = new DummyChangeStatusLogRepository();
        $service                = new RequestService($repository, $changeStatusRepository);
        $service->setUser($user);

        $creator = new User(2, 'Creator', new Role(Role::MANAGER));
        $request = new Request(1, $creator, 'name', 'description', new Status($status));
        if ($managerId !== null) {
            $request->setManager(new User($managerId, 'Manager', new Role(Role::MANAGER)));
        }

        $this->assertEquals($expected, $service->canSendForReview($request),
            "Check permission to send request for review: role={$role}, status={$status}, user={$user->getId()}, manager={$managerId}");
    }
}