<?php

declare(strict_types=1);

namespace tests\models;

use app\models\Action;
use app\models\ActionPermission;
use app\models\Role;
use Codeception\Test\Unit;


/**
 * Class ActionPermissionTest
 * @package tests\models
 */
class ActionPermissionTest extends Unit
{
    public function permissionsData(): array
    {
        return [
            [Action::CREATE, Role::MANAGER, true],
            [Action::CREATE, Role::SUPERVISOR, true],
            [Action::UPDATE, Role::MANAGER, false],
            [Action::UPDATE, Role::SUPERVISOR, true],
            [Action::DELETE, Role::MANAGER, false],
            [Action::DELETE, Role::SUPERVISOR, true],
            [Action::START_EXECUTION, Role::MANAGER, true],
            [Action::START_EXECUTION, Role::SUPERVISOR, false],
            [Action::SEND_FOR_REVIEW, Role::MANAGER, true],
            [Action::SEND_FOR_REVIEW, Role::SUPERVISOR, false],
            [Action::CLOSE, Role::MANAGER, false],
            [Action::CLOSE, Role::SUPERVISOR, true],
            [Action::READ_CHANGE_STATUS_LOG, Role::MANAGER, false],
            [Action::READ_CHANGE_STATUS_LOG, Role::SUPERVISOR, true],
        ];
    }

    /**
     * @dataProvider permissionsData
     *
     * @param string $action
     * @param string $role
     * @param bool   $expected
     */
    public function testPermissions(string $action, string $role, bool $expected): void
    {
        $actionPermission = new ActionPermission(new Action($action));

        $this->assertEquals($expected, $actionPermission->allowedToRole(new Role($role)),
            "Check permission to execute action: action={$action}, role={$role}");
    }
}