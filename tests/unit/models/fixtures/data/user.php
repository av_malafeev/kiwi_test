<?php

use tests\models\fixtures\UserFixture;

return [
    'manager'    => [
        'id'       => UserFixture::MANAGER_ID_1,
        'name'     => 'Manager1',
        'email'    => 'manager1@test.test',
        'password' => 'password',
        'role'     => 'MANAGER',
    ],
    'manager2'   => [
        'id'       => UserFixture::MANAGER_ID_2,
        'name'     => 'Manager2',
        'email'    => 'manager2@test.test',
        'password' => 'password',
        'role'     => 'MANAGER',
    ],
    'supervisor' => [
        'id'       => UserFixture::SUPERVISOR_ID,
        'name'     => 'Supervisor',
        'email'    => 'supervisor@test.test',
        'password' => 'password',
        'role'     => 'SUPERVISOR',
    ],
];