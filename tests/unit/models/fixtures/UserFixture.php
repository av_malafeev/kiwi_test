<?php

declare(strict_types=1);

namespace tests\models\fixtures;

use yii\test\ActiveFixture;

/**
 * Class UserFixture
 * @package tests\models\fixtures
 */
class UserFixture extends ActiveFixture
{
    const MANAGER_ID_1 = 1;
    const MANAGER_ID_2 = 2;
    const SUPERVISOR_ID = 3;

    public $tableName = 'user';
}
