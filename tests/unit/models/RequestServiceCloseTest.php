<?php

declare(strict_types=1);

namespace tests\models;

use app\models\exceptions\ForbiddenActionException;
use app\models\exceptions\InvalidRequestStatusException;
use app\models\Request;
use app\models\RequestService;
use app\models\Role;
use app\models\Status;
use app\models\User;
use Codeception\Test\Unit;
use tests\models\mocks\DummyChangeStatusLogRepository;
use tests\models\mocks\DummyRequestRepository;

/**
 * Class RequestServiceCloseTest
 * @package tests\models
 */
class RequestServiceCloseTest extends Unit
{
    public function closeData(): array
    {
        return [
            [Role::MANAGER, Status::CREATED, ForbiddenActionException::class],
            [Role::SUPERVISOR, Status::CREATED, InvalidRequestStatusException::class],
            [Role::SUPERVISOR, Status::IN_THE_WORK, InvalidRequestStatusException::class],
            [Role::SUPERVISOR, Status::ON_REVIEW, null],
            [Role::SUPERVISOR, Status::CLOSED, InvalidRequestStatusException::class],
        ];
    }

    /**
     * @dataProvider closeData
     *
     * @param string      $role
     * @param int         $status
     * @param null|string $expectedException
     */
    public function testClose(string $role, int $status, ?string $expectedException): void
    {
        if ($expectedException !== null) {
            $this->expectException($expectedException);
        }

        $repository             = new DummyRequestRepository();
        $changeStatusRepository = new DummyChangeStatusLogRepository();
        $service                = new RequestService($repository, $changeStatusRepository);
        $service->setUser(new User(1, 'User', new Role($role)));

        $creator = new User(2, 'Creator', new Role(Role::MANAGER));
        $request = new Request(1, $creator, 'name', 'description', new Status($status));

        $service->close($request);
    }

    public function canCloseData(): array
    {
        return [
            [Role::MANAGER, Status::CREATED, false],
            [Role::SUPERVISOR, Status::CREATED, false],
            [Role::SUPERVISOR, Status::IN_THE_WORK, false],
            [Role::SUPERVISOR, Status::ON_REVIEW, true],
            [Role::SUPERVISOR, Status::CLOSED, false],
        ];
    }

    /**
     * @dataProvider canCloseData
     *
     * @param string $role
     * @param int    $status
     * @param bool   $expected
     */
    public function testCanClose(string $role, int $status, bool $expected): void
    {
        $repository             = new DummyRequestRepository();
        $changeStatusRepository = new DummyChangeStatusLogRepository();
        $service                = new RequestService($repository, $changeStatusRepository);
        $service->setUser(new User(1, 'User', new Role($role)));

        $creator = new User(2, 'Creator', new Role(Role::MANAGER));
        $request = new Request(1, $creator, 'name', 'description', new Status($status));

        $this->assertEquals($expected, $service->canClose($request),
            "Check permission to close request: role={$role}, status={$status}");
    }
}