<?php

declare(strict_types=1);

namespace tests\models;

use app\models\exceptions\ForbiddenActionException;
use app\models\exceptions\InvalidRequestStatusException;
use app\models\Request;
use app\models\RequestService;
use app\models\Role;
use app\models\Status;
use app\models\User;
use Codeception\Test\Unit;
use tests\models\mocks\DummyChangeStatusLogRepository;
use tests\models\mocks\DummyRequestRepository;

/**
 * Class RequestServiceStartExecutionTest
 * @package tests\models
 */
class RequestServiceStartExecutionTest extends Unit
{
    public function startExecutionData(): array
    {
        return [
            [Role::SUPERVISOR, Status::CREATED, ForbiddenActionException::class],
            [Role::MANAGER, Status::CREATED, null],
            [Role::MANAGER, Status::IN_THE_WORK, InvalidRequestStatusException::class],
            [Role::MANAGER, Status::ON_REVIEW, InvalidRequestStatusException::class],
            [Role::MANAGER, Status::CLOSED, InvalidRequestStatusException::class],
        ];
    }

    /**
     * @dataProvider startExecutionData
     *
     * @param string      $role
     * @param int         $status
     * @param null|string $expectedException
     */
    public function testStartExecution(string $role, int $status, ?string $expectedException): void
    {
        if ($expectedException !== null) {
            $this->expectException($expectedException);
        }

        $repository             = new DummyRequestRepository();
        $changeStatusRepository = new DummyChangeStatusLogRepository();
        $service                = new RequestService($repository, $changeStatusRepository);
        $service->setUser(new User(1, 'User', new Role($role)));

        $creator = new User(2, 'Creator', new Role(Role::MANAGER));
        $request = new Request(1, $creator, 'name', 'description', new Status($status));

        $service->startExecution($request);
    }

    public function canStartExecutionData(): array
    {
        return [
            [Role::SUPERVISOR, Status::CREATED, false],
            [Role::MANAGER, Status::CREATED, true],
            [Role::MANAGER, Status::IN_THE_WORK, false],
            [Role::MANAGER, Status::ON_REVIEW, false],
            [Role::MANAGER, Status::CLOSED, false],
        ];
    }

    /**
     * @dataProvider canStartExecutionData
     *
     * @param string $role
     * @param int    $status
     * @param bool   $expected
     */
    public function testCanStartExecution(string $role, int $status, bool $expected): void
    {
        $repository             = new DummyRequestRepository();
        $changeStatusRepository = new DummyChangeStatusLogRepository();
        $service                = new RequestService($repository, $changeStatusRepository);
        $service->setUser(new User(1, 'User', new Role($role)));

        $creator = new User(2, 'Creator', new Role(Role::MANAGER));
        $request = new Request(1, $creator, 'name', 'description', new Status($status));

        $this->assertEquals($expected, $service->canStartExecution($request),
            "Check permission to start execution of request: role={$role}, status={$status}");
    }
}