<?php

declare(strict_types=1);

namespace tests\models;

use app\models\ChangeStatusLog;
use app\models\exceptions\ForbiddenActionException;
use app\models\RequestService;
use app\models\Role;
use app\models\Status;
use app\models\User;
use Codeception\Test\Unit;
use Codeception\Util\Debug;
use tests\models\mocks\ArrayChangeStatusLogRepository;
use tests\models\mocks\ArrayRequestRepository;
use tests\models\mocks\DummyChangeStatusLogRepository;
use tests\models\mocks\DummyRequestRepository;

/**
 * Class RequestServiceTest
 * @package tests\models
 */
class RequestServiceCreateTest extends Unit
{
    public function createData(): array
    {
        return [
            [Role::MANAGER, null],
            [Role::SUPERVISOR, null],
        ];
    }

    /**
     * @dataProvider createData
     *
     * @param string      $role
     * @param null|string $expectedException
     */
    public function testCreate(string $role, ?string $expectedException): void
    {
        if ($expectedException !== null) {
            $this->expectException($expectedException);
        }

        $repository             = new DummyRequestRepository();
        $changeStatusRepository = new DummyChangeStatusLogRepository();
        $service                = new RequestService($repository, $changeStatusRepository);
        $service->setUser(new User(1, 'User', new Role($role)));

        $service->create('name', 'description');
    }

    public function canCreateData(): array
    {
        return [
            [Role::MANAGER, true],
            [Role::SUPERVISOR, true],
        ];
    }

    /**
     * @dataProvider canCreateData
     *
     * @param string $role
     * @param bool   $expected
     */
    public function testCanCreate(string $role, bool $expected): void
    {
        $repository             = new DummyRequestRepository();
        $changeStatusRepository = new DummyChangeStatusLogRepository();
        $service                = new RequestService($repository, $changeStatusRepository);
        $service->setUser(new User(1, 'User', new Role($role)));

        $this->assertEquals($expected, $service->canCreate(), "Check permission to create request: role={$role}");
    }

    public function testCreatedRequest()
    {
        $repository             = new ArrayRequestRepository();
        $changeStatusRepository = new ArrayChangeStatusLogRepository();
        $service                = new RequestService($repository, $changeStatusRepository);

        $userId             = 1;
        $requestName        = 'name';
        $requestDescription = 'description';

        $service->setUser(new User($userId, 'User', new Role(Role::MANAGER)));
        $service->create($requestName, $requestDescription);

        $request = $repository->getById($repository->currentId);

        $this->assertEquals($userId, $request->getCreator()->getId(),
            "Request has invalid creator: userId=$userId, creatorId={$request->getCreator()->getId()}");
        $this->assertEquals($requestName, $request->getName(),
            "Request has invalid name: userId=$userId, creatorId={$request->getName()}");
        $this->assertEquals($requestDescription, $request->getDescription(),
            "Request has invalid description: userId=$requestDescription, creatorId={$request->getDescription()}");

        $models = $changeStatusRepository->getByRequestId($repository->currentId);

        /* @var $lastStatusChange ChangeStatusLog */
        end($models);
        $lastStatusChange = current($models);
        $this->assertEquals(Status::CREATED, $lastStatusChange->getStatus()->getStatusId(),
            "Incorrect status in last record after request was created: statusId={$lastStatusChange->getStatus()->getStatusId()}");
    }
}