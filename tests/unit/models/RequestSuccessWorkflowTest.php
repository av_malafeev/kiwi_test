<?php

declare(strict_types=1);

namespace tests\models;

use app\models\exceptions\RequestNotFoundException;
use app\models\repositories\SqlChangeStatusLogRepository;
use app\models\repositories\SqlRequestRepository;
use app\models\RequestService;
use app\models\Role;
use app\models\Status;
use app\models\User;
use Codeception\Test\Unit;
use tests\models\fixtures\UserFixture;

/**
 * Class RequestWorkflowTest
 * @package tests\models
 */
class RequestSuccessWorkflowTest extends Unit
{
    public function _fixtures()
    {
        return [
            'users' => UserFixture::class,
        ];
    }

    public function testWorkflow()
    {
        $manager1   = new User(UserFixture::MANAGER_ID_1, 'Manager1', new Role(Role::MANAGER));
        $manager2   = new User(UserFixture::MANAGER_ID_2, 'Manager2', new Role(Role::MANAGER));
        $supervisor = new User(UserFixture::SUPERVISOR_ID, 'Supervisor', new Role(Role::SUPERVISOR));

        $requestRepository         = new SqlRequestRepository();
        $changeStatusLogRepository = new SqlChangeStatusLogRepository();

        $requestService = new RequestService($requestRepository, $changeStatusLogRepository);
        $requestService->setUser($manager1);

        $requestName        = 'name';
        $requestDescription = 'description';

        $requestId = $requestService->create('name', 'description');
        $request   = $requestRepository->getById($requestId);

        $this->assertEquals($manager1->getId(), $request->getCreator()->getId(), 'Creator of request is incorrect');
        $this->assertEquals($requestName, $request->getName(), 'Name of request is incorrect');
        $this->assertEquals($requestDescription, $request->getDescription(), 'Description of request is incorrect');

        $requestService->setUser($manager2);
        $requestService->startExecution($request);
        $request = $requestRepository->getById($requestId);
        $this->assertEquals($manager2->getId(), $request->getManager()->getId(), 'Manager of request is incorrect');
        $this->assertEquals(Status::IN_THE_WORK, $request->getStatus()->getStatusId(),
            'Status of request is incorrect');

        $requestResult = 'result';
        $requestService->sendForReview($request, $requestResult);
        $request = $requestRepository->getById($requestId);
        $this->assertEquals($requestResult, $request->getResult(), 'Result of request is incorrect');
        $this->assertEquals(Status::ON_REVIEW, $request->getStatus()->getStatusId(), 'Status of request is incorrect');

        $requestService->setUser($supervisor);
        $requestService->close($request);
        $request = $requestRepository->getById($requestId);
        $this->assertEquals(Status::CLOSED, $request->getStatus()->getStatusId(), 'Status of request is incorrect');

        $requestName        = 'updated name';
        $requestDescription = 'updated description';
        $requestResult      = 'updated result';
        $requestService->update($request, $requestName, $requestDescription, $requestResult);
        $request = $requestRepository->getById($requestId);
        $this->assertEquals($requestName, $request->getName(), 'Name of request is incorrect');
        $this->assertEquals($requestDescription, $request->getDescription(), 'Description of request is incorrect');
        $this->assertEquals($requestResult, $request->getResult(), 'Result of request is incorrect');

        $requestService->delete($request);
        $this->expectException(RequestNotFoundException::class);
        $request = $requestRepository->getById($requestId);
    }

}