<?php

declare(strict_types=1);

namespace tests\models;

use app\models\exceptions\ForbiddenActionException;
use app\models\exceptions\InvalidParamException;
use app\models\Request;
use app\models\RequestService;
use app\models\Role;
use app\models\Status;
use app\models\User;
use Codeception\Test\Unit;
use tests\models\mocks\ArrayChangeStatusLogRepository;
use tests\models\mocks\ArrayRequestRepository;
use tests\models\mocks\DummyChangeStatusLogRepository;
use tests\models\mocks\DummyRequestRepository;

/**
 * Class RequestServiceUpdateTest
 * @package tests\models
 */
class RequestServiceUpdateTest extends Unit
{
    public function updateData(): array
    {
        return [
            [Role::MANAGER, Status::CREATED, null, ForbiddenActionException::class],
            [Role::SUPERVISOR, Status::CREATED, null, null],
            [Role::SUPERVISOR, Status::CREATED, null, null],
            [Role::SUPERVISOR, Status::IN_THE_WORK, 'result', InvalidParamException::class],
            [Role::SUPERVISOR, Status::IN_THE_WORK, 'result', InvalidParamException::class],
            [Role::SUPERVISOR, Status::ON_REVIEW, 'result', null],
            [Role::SUPERVISOR, Status::CLOSED, 'result', null],
        ];
    }

    /**
     * @dataProvider updateData
     *
     * @param string      $role
     * @param int         $status
     * @param null|string $result
     * @param null|string $expectedException
     */
    public function testUpdate(string $role, int $status, ?string $result, ?string $expectedException): void
    {
        if ($expectedException !== null) {
            $this->expectException($expectedException);
        }

        $repository             = new DummyRequestRepository();
        $changeStatusRepository = new DummyChangeStatusLogRepository();
        $service                = new RequestService($repository, $changeStatusRepository);
        $service->setUser(new User(1, 'User', new Role($role)));

        $creator = new User(2, 'Creator', new Role(Role::MANAGER));
        $request = new Request(1, $creator, 'name', 'description', new Status($status));
        $service->update($request, 'name', 'description', $result);
    }

    public function canUpdateData(): array
    {
        return [
            [Role::MANAGER, false],
            [Role::SUPERVISOR, true],
        ];
    }

    /**
     * @dataProvider canUpdateData
     *
     * @param string $role
     * @param bool   $expected
     */
    public function testCanUpdate(string $role, bool $expected): void
    {
        $repository             = new DummyRequestRepository();
        $changeStatusRepository = new DummyChangeStatusLogRepository();
        $service                = new RequestService($repository, $changeStatusRepository);
        $service->setUser(new User(1, 'User', new Role($role)));

        $this->assertEquals($expected, $service->canUpdate(), "Check permission to update request: role={$role}");
    }

}