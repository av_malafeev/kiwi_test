<?php

declare(strict_types=1);

namespace tests\models;

use app\models\Request;
use app\models\Role;
use app\models\Status;
use app\models\User;
use Codeception\Test\Unit;

/**
 * Class RequestTest
 * @package tests\models
 */
class RequestTest extends Unit
{
    public function testIsManager(): void
    {
        $manager      = new User(1, 'Manager', new Role(Role::MANAGER));
        $otherManager = new User(2, 'Other manager', new Role(Role::MANAGER));

        $request = new Request(1, $manager, 'Name', 'Description', new Status(Status::IN_THE_WORK));
        $request->setManager($otherManager);

        $this->assertFalse($request->isManager($manager),
            "User is manager of this request: user={$manager->getId()}, manager={$request->getManager()->getId()}");
        $this->assertTrue($request->isManager($otherManager),
            "User is not manager of this request: user={$manager->getId()}, manager={$request->getManager()->getId()}");
    }

    public function IsAllowedUpdateResultData(): array
    {
        return [
            [Status::CREATED, false],
            [Status::IN_THE_WORK, false],
            [Status::ON_REVIEW, true],
            [Status::CLOSED, true],
        ];
    }

    /**
     * @dataProvider IsAllowedUpdateResultData
     *
     * @param int  $status
     * @param bool $expected
     */
    public function testIsAllowedUpdateResult(int $status, bool $expected): void
    {
        $request = new Request(1, new User(1, 'User', new Role(Role::MANAGER)), 'name', 'description',
            new Status($status));

        $this->assertEquals($expected, $request->isAllowedUpdateResult());
    }
}