<?php

declare(strict_types=1);

namespace tests\models;

use app\models\exceptions\ForbiddenActionException;
use app\models\Request;
use app\models\RequestService;
use app\models\Role;
use app\models\Status;
use app\models\User;
use Codeception\Test\Unit;
use tests\models\mocks\DummyChangeStatusLogRepository;
use tests\models\mocks\DummyRequestRepository;

/**
 * Class RequestServiceDeleteTest
 * @package tests\models
 */
class RequestServiceDeleteTest extends Unit
{
    public function deleteData(): array
    {
        return [
            [Role::MANAGER, Status::CREATED, ForbiddenActionException::class],
            [Role::SUPERVISOR, Status::CREATED, null],
            [Role::SUPERVISOR, Status::IN_THE_WORK, null],
            [Role::SUPERVISOR, Status::ON_REVIEW, null],
            [Role::SUPERVISOR, Status::CLOSED, null],
        ];
    }

    /**
     * @dataProvider deleteData
     *
     * @param string      $role
     * @param null|string $expectedException
     */
    public function testDelete(string $role, int $status, ?string $expectedException): void
    {
        if ($expectedException !== null) {
            $this->expectException($expectedException);
        }

        $repository             = new DummyRequestRepository();
        $changeStatusRepository = new DummyChangeStatusLogRepository();
        $service                = new RequestService($repository, $changeStatusRepository);
        $service->setUser(new User(1, 'User', new Role($role)));

        $creator = new User(2, 'Creator', new Role(Role::MANAGER));
        $request = new Request(1, $creator, 'name', 'description', new Status($status));

        $service->delete($request);
    }

    public function canDeleteData(): array
    {
        return [
            [Role::MANAGER, false],
            [Role::SUPERVISOR, true],
        ];
    }

    /**
     * @dataProvider canDeleteData
     *
     * @param string $role
     * @param bool   $expected
     */
    public function testCanDelete(string $role, bool $expected): void
    {
        $repository             = new DummyRequestRepository();
        $changeStatusRepository = new DummyChangeStatusLogRepository();
        $service                = new RequestService($repository, $changeStatusRepository);
        $service->setUser(new User(1, 'User', new Role($role)));

        $this->assertEquals($expected, $service->canDelete(), "Check permission to delete request: role={$role}");
    }
}