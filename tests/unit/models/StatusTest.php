<?php

declare(strict_types=1);

namespace tests\models;

use app\models\Status;
use Codeception\Test\Unit;

/**
 * Class StatusTest
 * @package tests\models
 */
class StatusTest extends Unit
{
    public function canChangeToData(): array
    {
        return [
            [Status::CREATED, Status::CREATED, false],
            [Status::CREATED, Status::IN_THE_WORK, true],
            [Status::CREATED, Status::ON_REVIEW, false],
            [Status::CREATED, Status::CLOSED, false],

            [Status::IN_THE_WORK, Status::CREATED, false],
            [Status::IN_THE_WORK, Status::IN_THE_WORK, false],
            [Status::IN_THE_WORK, Status::ON_REVIEW, true],
            [Status::IN_THE_WORK, Status::CLOSED, false],

            [Status::ON_REVIEW, Status::CREATED, false],
            [Status::ON_REVIEW, Status::IN_THE_WORK, false],
            [Status::ON_REVIEW, Status::ON_REVIEW, false],
            [Status::ON_REVIEW, Status::CLOSED, true],

            [Status::CLOSED, Status::CREATED, false],
            [Status::CLOSED, Status::IN_THE_WORK, false],
            [Status::CLOSED, Status::ON_REVIEW, false],
            [Status::CLOSED, Status::CLOSED, false],
        ];
    }

    /**
     * @dataProvider canChangeToData
     *
     * @param int  $current
     * @param int  $next
     * @param bool $expected
     */
    public function testCanChangeTo(int $current, int $next, bool $expected): void
    {
        $status = new Status($current);

        $this->assertEquals($expected, $status->canChangeTo(new Status($next)),
            "Check change request status: from={$current}, to={$next}");
    }

}