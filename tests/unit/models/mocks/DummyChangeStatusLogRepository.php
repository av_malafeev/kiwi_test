<?php

declare(strict_types=1);

namespace tests\models\mocks;

use app\models\ChangeStatusLog;
use app\models\repositories\ChangeStatusLogRepository;

/**
 * Class DummyChangeStatusLogRepository
 * @package tests\models\mocks
 */
class DummyChangeStatusLogRepository implements ChangeStatusLogRepository
{
    private $models = [];

    public function getByRequestId(int $requestId): array
    {
        if (array_key_exists((string)$requestId, $this->models)) {
            return [];
        }

        return $this->models[(string)$requestId];
    }

    public function save(ChangeStatusLog $changeStatusLog): void
    {
        $this->models[(string)$changeStatusLog->getRequestId()] = $changeStatusLog;
    }
}