<?php

declare(strict_types=1);

namespace tests\models\mocks;

use app\models\repositories\RequestRepository;
use app\models\Request;

/**
 * Class DummyRequestRepository
 * @package tests\models\mocks
 */
class DummyRequestRepository implements RequestRepository
{
    public function save(Request $request): int
    {
        return 1;
    }

    public function getById(int $id): Request
    {
    }

    public function delete(Request $request): void
    {
    }
}