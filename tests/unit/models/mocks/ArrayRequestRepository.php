<?php

declare(strict_types=1);

namespace tests\models\mocks;

use app\models\exceptions\RequestNotFoundException;
use app\models\repositories\RequestRepository;
use app\models\Request;

/**
 * Class ArrayRequestRepository
 * @package tests\models\mocks
 */
class ArrayRequestRepository implements RequestRepository
{
    public $currentId = 0;

    private $requests = [];

    public function save(Request $request): int
    {
        $id                          = $request->getId() ?? ++$this->currentId;
        $this->requests[(string)$id] = $request;

        return $id;
    }

    public function getById(int $id): Request
    {
        if (!array_key_exists((string)$id, $this->requests)) {
            throw new RequestNotFoundException("Request not found. id={$id}");
        }

        return $this->requests[(string)$id];
    }

    public function delete(Request $request): void
    {
        if (!array_key_exists((string)$request->getId(), $this->requests)) {
            throw new RequestNotFoundException("Request not found. id={$request->getId()}");
        }

        unset($this->requests[(string)$request->getId()]);
    }
}