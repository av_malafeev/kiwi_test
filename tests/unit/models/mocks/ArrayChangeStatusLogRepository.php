<?php

declare(strict_types=1);

namespace tests\models\mocks;

use app\models\ChangeStatusLog;
use app\models\repositories\ChangeStatusLogRepository;

/**
 * Class ArrayChangeStatusLogRepository
 * @package tests\models\mocks
 */
class ArrayChangeStatusLogRepository implements ChangeStatusLogRepository
{
    private $models = [];

    public function getByRequestId(int $requestId): array
    {
        $id = (string)$requestId;

        return array_key_exists($id, $this->models) ? $this->models[$id] : [];
    }

    public function save(ChangeStatusLog $changeStatusLog): void
    {
        $this->models[(string)$changeStatusLog->getRequestId()][] = $changeStatusLog;
    }
}